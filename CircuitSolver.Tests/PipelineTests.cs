﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace CircuitSolver.Tests
{
    [TestClass]
    public class PipelineTests
    {
        [TestMethod]
        public void Example_circuits_simplify_and_output()
        {
            string[] examples = Directory.GetFiles("Examples");
            var sb = new StringBuilder();
            foreach (string file in examples)
            {
                Console.WriteLine("Testing input: " + file);
                using (TextReader reader = File.OpenText(file))
                {
                    ElectricalCircuit circuit = Program.InputCircuit(reader);
                    circuit.Simplify();
                    Program.ProduceOutput(circuit, sb);
                }

                sb.Clear();
            }
        }
    }
}
