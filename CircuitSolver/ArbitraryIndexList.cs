﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    /// <summary>
    /// Represents a list, into which items can be inserted at arbitrary (non-negative) indices.
    /// if the index is outside the current list, default values will be inserted into the vacant positions.
    /// </summary>
    public class ArbitraryIndexList<T> : List<T>
    {
        public new T this[int index]
        {
            set
            {
                if (index >= this.Count)
                {
                    for (int i = this.Count; i <= index; i++)
                        this.Add(default(T));
                }

                base[index] = value;
            }
            get => index < Count ? base[index] : default(T);
        }
    }
}
