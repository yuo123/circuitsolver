﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    public abstract class CircuitComponent
    {
        /// <summary>
        /// The indices of this component in the circuit.
        /// This is represented by the subscripts in the symbolic representation of the component.
        /// </summary>
        public abstract IEnumerable<int> Indices { get; }

        public CircuitConnection End1 { get; set; }
        public CircuitConnection End2 { get; set; }

        public DisplayDouble Resistance { get; protected set; }
        public DisplayDouble Current { get; set; }
        public virtual DisplayDouble Voltage => Resistance * Current;
        public virtual DisplayDouble Power => Voltage * Current;

        public virtual string IndicesString => "{" + string.Join(",", Indices) + "}";
        public virtual string CurrentSymbol => "I_" + IndicesString;
        public virtual string ResistanceSymbol => "R_" + IndicesString;
        public virtual string VoltageSymbol => "V_" + IndicesString;
        public virtual string PowerSymbol => "P_" + IndicesString;

        /// <summary>
        /// Calculates the resistance of this component and its sub-components,
        /// and set <see cref="Resistance"/> to it.
        /// </summary>
        public abstract void CalcResistance(StringBuilder latexOut);
        /// <summary>
        /// Calculates the currents going through this component's sub-components,
        /// and sets their <see cref="Current"/> properties.
        /// <para>
        /// This component's <see cref="Current"/> should be set prior to calling this method.
        /// </para>
        /// </summary>
        public abstract void CalcChildCurrent(StringBuilder latexOut);

        public virtual void CalcVoltage(StringBuilder latexOut)
        {
            string sIndices = IndicesString;
            latexOut.AppendLatexLine($"V_{sIndices} = R_{sIndices} I_{sIndices} = {Resistance} \\cdot {Current} = {Voltage}V");
        }

        public virtual void CalcPower(StringBuilder latexOut)
        {
            string sIndices = IndicesString;
            latexOut.AppendLatexLine($"P_{sIndices} = V_{sIndices} I_{sIndices} = {Voltage} \\cdot {Current} = {Voltage * Current}W");
        }

        public void Connect(CircuitConnection connection, CircuitConnection previous = null)
        {
            if (End1 == previous)
            {
                End1?.Remove(this);
                End1 = connection;
            }
            else if (End2 == previous)
            {
                End2?.Remove(this);
                End2 = connection;
            }
            else
            {
                throw new InvalidOperationException("Cannot replace component connection that doesn't exist.");
            }

            connection.Add(this);
        }
    }
}
