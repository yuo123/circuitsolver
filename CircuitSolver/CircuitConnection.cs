﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    /// <summary>
    /// Represents a connection (usually a wire) between multiple components in an electrical circuit
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{ToString()}")]
    public class CircuitConnection : List<CircuitComponent>
    {
        public override string ToString()
        {
            return "Connection: " + string.Join(", ", this);
        }
    }
}
