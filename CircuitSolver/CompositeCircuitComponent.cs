﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    [DebuggerDisplay("{ToString()}")]
    public abstract class CompositeCircuitComponent : CircuitComponent
    {
        public IEnumerable<CircuitComponent> Subcomponents { get; }

        public override IEnumerable<int> Indices => Subcomponents.SelectMany(comp => comp.Indices).OrderBy(i => i);

        protected CompositeCircuitComponent(IEnumerable<CircuitComponent> subcomponents)
            => this.Subcomponents = subcomponents;

        public override void CalcResistance(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
                comp.CalcResistance(latexOut);
        }

        public override void CalcChildCurrent(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
                comp.CalcChildCurrent(latexOut);
        }

        public override void CalcVoltage(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
                comp.CalcVoltage(latexOut);
        }

        public override void CalcPower(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
                comp.CalcPower(latexOut);
        }

        public override string ToString()
            => "(" + string.Join(", ", Subcomponents) + ")";
    }
}
