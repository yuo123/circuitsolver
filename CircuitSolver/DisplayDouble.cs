﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    /// <summary>
    /// A <see cref="DisplayDouble"/> wrapper, which can be used exactly the same, but for which a global format can be specified.
    /// </summary>
    public struct DisplayDouble
    {
        /// <summary>
        /// A format string to be passed to <see cref="DisplayDouble.ToString(string)"/> when <see cref="DisplayDouble.ToString"/> is called
        /// </summary>
        public static string Format { get; set; }

        public double Value { get; }

        public DisplayDouble(double value) : this() => Value = value;

        public override string ToString() => Value.ToString(Format);

        public static implicit operator double(DisplayDouble dd) => dd.Value;
        public static implicit operator DisplayDouble(double d) => new DisplayDouble(d);

        public static DisplayDouble operator +(DisplayDouble a, DisplayDouble b) => a.Value + b.Value;
        public static DisplayDouble operator -(DisplayDouble a, DisplayDouble b) => a.Value - b.Value;
        public static DisplayDouble operator /(DisplayDouble a, DisplayDouble b) => a.Value / b.Value;
        public static DisplayDouble operator *(DisplayDouble a, DisplayDouble b) => a.Value * b.Value;

        public static bool operator <(DisplayDouble a, DisplayDouble b) => a.Value < b.Value;
        public static bool operator >(DisplayDouble a, DisplayDouble b) => a.Value > b.Value;
        public static bool operator <=(DisplayDouble a, DisplayDouble b) => a.Value <= b.Value;
        public static bool operator >=(DisplayDouble a, DisplayDouble b) => a.Value >= b.Value;
    }
}
