﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    public class ElectricalCircuit
    {
        public CircuitConnection PositiveTerminal { get; set; }
        public CircuitConnection NegativeTerminal { get; set; }
        public IEnumerable<Resistor> AllResistors { get; }

        public DisplayDouble TerminalsVoltage { get; set; }

        public ElectricalCircuit(IEnumerable<Resistor> allResistors = null)
            => this.AllResistors = allResistors;

        public void Simplify()
        {
            while (!IsSimple())
                SimplifyStep();
        }

        public bool IsSimple()
        {
            return PositiveTerminal.Count == 1 && NegativeTerminal.Count == 1 && PositiveTerminal[0] == NegativeTerminal[0];
        }

        public void CalcVoltage(StringBuilder latexOut)
        {
            if (!IsSimple())
                throw new InvalidOperationException("Cannot calculate voltage on non-simplified circuit");

            PositiveTerminal[0].CalcVoltage(latexOut);
        }

        public void CalcPower(StringBuilder latexOut)
        {
            if (!IsSimple())
                throw new InvalidOperationException("Cannot calculate power for non-simplified circuit");

            CircuitComponent total = PositiveTerminal[0];

            total.CalcPower(latexOut);
            latexOut.AppendLatexLine($"P_T = \\varepsilon I_T = {TerminalsVoltage} \\cdot {total.Current} = {total.Power}W");
            latexOut.AppendJoin(" + ", AllResistors.Select(resistor => resistor.Power.ToString()))
                .Append(" \\stackrel{\\checkmark}{=} ").AppendLine(total.Power.ToString());
        }

        public void CalcChildCurrent(StringBuilder latexOut)
        {
            if (!IsSimple())
                throw new InvalidOperationException("Cannot calculate current through non-simplified circuit");

            CircuitComponent total = PositiveTerminal[0];

            total.Current = TerminalsVoltage / total.Resistance;
            latexOut.AppendLatexLine("I_T = \\frac{\\varepsilon}{R_T} = \\frac{" + TerminalsVoltage + "}{" + total.Resistance + "} = " + total.Current + "A");
            total.CalcChildCurrent(latexOut);
        }

        public void CalcResistance(StringBuilder latexOut)
        {
            if (!IsSimple())
                throw new InvalidOperationException("Cannot calculate resistance of non-simplified circuit");

            PositiveTerminal[0].CalcResistance(latexOut);
        }

        private void SimplifyStep()
        {
            var visited = new HashSet<CircuitConnection>();
            var todo = new Stack<CircuitConnection>();
            todo.Push(PositiveTerminal);

            while (todo.Count > 0)
            {
                CircuitConnection cur = todo.Pop();
                if (!visited.Contains(cur))
                {
                    visited.Add(cur);
                    SimplifySerial(cur);
                    SimplifyParallel(cur);
                    todo.PushMany(cur.Select(comp => Next(comp, via: cur)));
                }
            }
        }

        /// <summary>
        /// Tries to simplify a serial composite from the given connection
        /// </summary>
        /// <returns>Whether any simplification occurred</returns>
        private bool SimplifySerial(CircuitConnection conn)
        {
            bool re = false;

            CircuitConnection[] nexts = conn.Select(c => Next(c, conn)).ToArray(conn.Count);
            for (int i = 0; i < nexts.Length; i++)
            {
                var comps = new List<CircuitComponent> { conn[i] };
                CircuitConnection cur = nexts[i];
                CircuitComponent via = conn[i];

                //go down stream until we meet a split or a dead end
                while (cur.Count == 2 && cur != PositiveTerminal && cur != NegativeTerminal)
                {
                    CircuitComponent next = Next(cur, via).First();
                    comps.Add(next);
                    cur = Next(next, cur);
                    via = next;
                }
                //now, conn is right before the chain and cur is right after

                //detach the individual components, and insert the composite one
                if (comps.Count > 1)
                {
                    new SerialComposite(comps, conn, cur);
                    re = true;
                }
            }

            return re;
        }

        /// <summary>
        /// Tries to simplify a parallel composite from the given connection
        /// </summary>
        /// <returns>Whether any simplification occurred</returns>
        private bool SimplifyParallel(CircuitConnection conn)
        {
            CircuitConnection[] nexts = conn.Select(c => Next(c, conn)).ToArray(conn.Count);
            for (int i = 0; i < nexts.Length - 1; i++)
            {
                List<CircuitComponent> comps = null;
                for (int j = i + 1; j < nexts.Length; j++)
                {
                    if (nexts[i] == nexts[j])
                    {
                        (comps ?? (comps = new List<CircuitComponent>(2) { conn[i] }))
                           .Add(conn[j]);
                    }
                }
                //create the composite component
                if (comps != null)
                {
                    new ParallelComposite(comps, conn, nexts[i]);
                    return true;
                }
            }
            return false;
        }

        private IEnumerable<CircuitComponent> Next(CircuitConnection conn, CircuitComponent via)
            => conn.FindAll(c => c != via);

        private CircuitConnection Next(CircuitComponent comp, CircuitConnection via)
        {
            if (comp.End1 == via)
                return comp.End2;
            return comp.End1;
        }
    }
}
