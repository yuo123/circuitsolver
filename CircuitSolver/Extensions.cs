﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    public static class Extensions
    {
        public static T[] ToArray<T>(this IEnumerable<T> source, int size)
        {
            T[] re = new T[size];
            int i = 0;
            foreach (T item in source)
                re[i++] = item;

            return re;
        }

        public static void RemoveRange<T>(this IList<T> source, IEnumerable<T> range)
        {
            foreach (T item in range)
                source.Remove(item);
        }

        /// <summary>
        /// Replaces the first occurrence of the given element in <paramref name="source"/> with <paramref name="newItem"/>
        /// </summary>
        public static void Replace<T>(this IList<T> source, T oldItem, T newItem)
            => source[source.IndexOf(oldItem)] = newItem;

        public static void PushMany<T>(this Stack<T> stack, IEnumerable<T> items)
        {
            foreach (T item in items)
                stack.Push(item);
        }

        /// <summary>
        /// Appends a copy of <paramref name="value"/> to <paramref name="stringBuilder"/>, followed by "<c> \\</c>" and the default line terminator.
        /// </summary>
        public static void AppendLatexLine(this StringBuilder stringBuilder, string value = "")
            => stringBuilder.Append(value).AppendLine(" \\\\");

        /// <summary>
        /// Appends an <see cref="IEnumerable{T}"/> of strings to a <see cref="StringBuilder"/>, with a separator between them
        /// </summary>
        public static StringBuilder AppendJoin(this StringBuilder stringBuilder, string separator, IEnumerable<string> values)
        {
            stringBuilder.Append(values.First());
            foreach (string value in values.Skip(1))
                stringBuilder.Append(separator).Append(value);
            return stringBuilder;
        }

        public static DisplayDouble DD(this double value) => value;
    }
}
