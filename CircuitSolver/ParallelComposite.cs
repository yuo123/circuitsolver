﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    public class ParallelComposite : CompositeCircuitComponent
    {
        public ParallelComposite(IEnumerable<CircuitComponent> subcomponents) : base(subcomponents) { }
        /// <summary>
        /// Initializes a new ParallelComposite with the given sub-components, between two given connections
        /// </summary>
        /// <param name="subcomponents">
        /// An <see cref="IList{T}"/> of components comprising this composite component. 
        /// All elements must be connected to both <paramref name="end1"/> and <paramref name="end2"/>
        /// </param>
        public ParallelComposite(IList<CircuitComponent> subcomponents, CircuitConnection end1, CircuitConnection end2)
            : this(subcomponents)
        {
            end1.RemoveRange(subcomponents);
            end2.RemoveRange(subcomponents);
            Connect(end1);
            Connect(end2);
        }

        public override void CalcChildCurrent(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
            {
                comp.Current = (this.Resistance / comp.Resistance) * this.Current;
                latexOut.Append(comp.CurrentSymbol).Append(" = ")
                    .Append("\\frac{").Append(this.ResistanceSymbol).Append("}{").Append(comp.ResistanceSymbol).Append("}").Append(this.CurrentSymbol).Append(" = ")
                    .Append("\\frac{").Append(this.Resistance).Append("}{").Append(comp.Resistance).Append("}").Append(" \\cdot ").Append(this.Current).Append(" = ")
                    .Append((object)comp.Current).Append("A");

                latexOut.AppendLatexLine();
            }

            //calc children's children
            base.CalcChildCurrent(latexOut);
        }

        public override void CalcResistance(StringBuilder latexOut)
        {
            //calc children
            base.CalcResistance(latexOut);

            this.Resistance = 1.0 / Subcomponents.Sum(comp => 1.0 / comp.Resistance);

            latexOut.Append("\\frac{1}{").Append(ResistanceSymbol).Append("} = ")
                .Append(string.Join(" + ", Subcomponents.Select(comp => "\\frac{1}{" + comp.ResistanceSymbol + "}"))).Append(" = ")
                .Append(string.Join(" + ", Subcomponents.Select(comp => "\\frac{1}{" + comp.Resistance + "}"))).Append(" = ")
                .Append("\\frac{1}{").Append((object)Resistance).AppendLatexLine("\\Omega}");
        }

        public override string ToString() => "Parallel: " + base.ToString();
    }
}
