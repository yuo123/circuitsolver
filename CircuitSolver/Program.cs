﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

namespace CircuitSolver
{
    internal static class Program
    {
        private const string OUT = "out.tex";

        #region Header & Footer

        private const string HEADER =
@"\documentclass[fleqn]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage[a4paper, vmargin=1.5cm]{geometry}

\allowdisplaybreaks

\begin{document}

\begin{gather*}
";

        private const string FOOTER =
@"\end{gather*}

\end{document}";

        #endregion

        private static void Main(string[] args)
        {
            ElectricalCircuit circuit = InputCircuit();

            var sw = new Stopwatch();
            sw.Start();
            circuit.Simplify();
            sw.Stop();
            Console.WriteLine($"\r\nSimplification took {sw.ElapsedMilliseconds} ms");

            var sb = new StringBuilder();
            ProduceOutput(circuit, sb);
            DumpOutput(args, sb.ToString());
        }

        public static void ProduceOutput(ElectricalCircuit circuit, StringBuilder sb)
        {
            DisplayDouble.Format = "0.##";

            sb.Append(HEADER);
            circuit.CalcResistance(sb);
            sb.AppendLine(" \\\\ \\\\");
            circuit.CalcChildCurrent(sb);
            sb.AppendLine(" \\\\ \\\\");
            circuit.CalcVoltage(sb);
            sb.AppendLine(" \\\\ \\\\");
            circuit.CalcPower(sb);
            sb.Append(FOOTER);
        }

        private static void DumpOutput(string[] args, string output)
        {
            string outfile = args.Length > 0 ? args[0] : OUT;
            File.WriteAllText(outfile, output);
            if (args.Length > 1)
            {
                string compiler = args[1];
                if (File.Exists(compiler))
                {
                    Process.Start(compiler, outfile);
                    Console.WriteLine($"Compiled to PDF file: {Path.GetDirectoryName(outfile) + Path.GetFileNameWithoutExtension(outfile)}.pdf");
                }
                else
                {
                    Console.WriteLine("Supplied PDF compiler path not found: " + compiler);
                }
            }
        }

        public static ElectricalCircuit InputCircuit() => InputCircuit(Console.In);
        public static ElectricalCircuit InputCircuit(TextReader inputReader)
        {
            var resistors = new ArbitraryIndexList<Resistor>();
            var circuit = new ElectricalCircuit(resistors);
            string input;

            Console.WriteLine("Input connections by writing each in a separate line.");
            Console.WriteLine("Each line can contain integers (representing resistor indices), + (positive terminal), or - (negative terminal), separated by commas.");
            Console.WriteLine("Enter a blank line to finish:");
            while ((input = inputReader.ReadLine()) != string.Empty)
            {
                var conn = new CircuitConnection();
                foreach (string item in input.Split(','))
                {
                    switch (item.Trim())
                    {
                        case "+":
                            circuit.PositiveTerminal = conn;
                            break;
                        case "-":
                            circuit.NegativeTerminal = conn;
                            break;
                        default:
                            int index = int.Parse(item) - 1;
                            //initialize resistor if doesn't exist, and connect it
                            (resistors[index] ?? (resistors[index] = new Resistor(index + 1))).Connect(conn);
                            break;
                    }
                }
            }

            if (resistors.Any(r => r.End1 == null || r.End2 == null))
                Console.WriteLine("Warning: not all resistors are connected on both sides.\r\n");

            Console.WriteLine("Input resistance values, in Ohms:");
            for (int i = 0; i < resistors.Count; i++)
            {
                Console.Write($"R{i + 1}: ");
                resistors[i].SetResistance(double.Parse(inputReader.ReadLine()));
            }

            Console.Write("Terminals voltage (Volts): ");
            circuit.TerminalsVoltage = double.Parse(inputReader.ReadLine());
            return circuit;
        }
    }
}
