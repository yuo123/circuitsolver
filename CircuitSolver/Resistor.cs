﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    [System.Diagnostics.DebuggerDisplay("{ToString()}: {Resistance}Ω")]
    public class Resistor : CircuitComponent
    {
        public int Index { get; }
        public override IEnumerable<int> Indices { get { yield return Index; } }

        public Resistor(int index)
            => this.Index = index;

        public void SetResistance(DisplayDouble value)
            => this.Resistance = value;

        // No children
        public override void CalcChildCurrent(StringBuilder latexOut) { }

        // Resistance already set
        public override void CalcResistance(StringBuilder latexOut)
            => latexOut.Append(ResistanceSymbol).Append(" = ").Append((object)Resistance).AppendLatexLine("\\Omega");

        public override string ToString()
            => "R" + Index;
    }
}
