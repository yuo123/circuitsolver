﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircuitSolver
{
    public class SerialComposite : CompositeCircuitComponent
    {
        public SerialComposite(IEnumerable<CircuitComponent> subcomponents) : base(subcomponents) { }

        /// <summary>
        /// Initializes a new SerialComposite with the given sub-components, between two given connections
        /// </summary>
        /// <param name="subcomponents">
        /// An <see cref="IList{T}"/> of components comprising this serial composite component. 
        /// The first element must be connected to <paramref name="end1"/>, and the second to <paramref name="end2"/>
        /// </param>
        public SerialComposite(IList<CircuitComponent> subcomponents, CircuitConnection end1, CircuitConnection end2)
            : this(subcomponents)
        {
            end1.Replace(subcomponents[0], this);
            end2.Replace(subcomponents[subcomponents.Count - 1], this);
            this.End1 = end1;
            this.End2 = end2;
        }

        public override void CalcChildCurrent(StringBuilder latexOut)
        {
            foreach (CircuitComponent comp in Subcomponents)
            {
                comp.Current = this.Current;
                latexOut.Append(comp.CurrentSymbol).Append(" = ");
            }
            latexOut.Append((object)Current).AppendLatexLine("A");

            //calc children's children
            base.CalcChildCurrent(latexOut);
        }

        public override void CalcResistance(StringBuilder latexOut)
        {
            //calc children
            base.CalcResistance(latexOut);

            this.Resistance = Subcomponents.Sum(comp => comp.Resistance);

            latexOut.Append(ResistanceSymbol).Append(" = ")
                .Append(string.Join(" + ", Subcomponents.Select(comp => comp.ResistanceSymbol))).Append(" = ")
                .Append(string.Join(" + ", Subcomponents.Select(comp => comp.Resistance))).Append(" = ")
                .Append((object)Resistance).AppendLatexLine("\\Omega");
        }

        public override string ToString()
            => "Serial: " + base.ToString();
    }
}
